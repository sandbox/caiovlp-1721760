<?php

class DrupalOrgCrawlerUser
{
  private $_doc = NULL;

  function __construct($html) {
    $this->_doc = new DOMDocument();
    @$this->_doc->loadHTML($html);
  }

  function getUsername() {
    $status = NULL;
    $xpath = new DOMXpath($this->_doc);
    $matches = $xpath->query("//h1");
    if ($matches->length > 0) {
      $status = trim($matches->item(0)->nodeValue);
    }

    return $status;
  }

  function getFullname() {
    $author = NULL;
    $xpath = new DOMXpath($this->_doc);
    $matches = $xpath->query("//div[contains(@class, 'profile')]//dd[contains(@class, 'profile-profile_full_name')]");
    if ($matches->length > 0) {
      $author = trim($matches->item(0)->nodeValue);
    }

    return $author;
  }

  function getCommitCount() {
    $count = 0;
    $xpath = new DOMXpath($this->_doc);
    $matches = $xpath->query("//div[contains(@class, 'profile')]//dl[contains(@class, 'versioncontrol-project-user-commits')]//li[contains(@class, 'last')]");
    if ($matches->length > 0) {
      $count = preg_replace('/Total:\s([0-9]+)\scommit[s]*/', '$1', $matches->item(0)->nodeValue);
    }

    return $count;
  }

  function getCreatedTimestamp() {
    $created = NULL;
    $xpath = new DOMXpath($this->_doc);
    $matches = $xpath->query("//div[contains(@class, 'profile')]//dl[contains(@class, 'user-member')]/dd");
    if ($matches->length > 0) {
      // Subtract the "member for" field from now.
      $created = strtotime('-' . $matches->item(0)->nodeValue);
    }

    return $created;
  }

  private function parseDate($date) {
    $matches = array();
    preg_match('/([a-zA-Z]+)\s+([0-9]+),\s+([0-9]+)\sat\s(.*)/', $date, $matches);

    $date = $matches[1] . '-' . $matches[2] . '-' . $matches[3] . ' ' . $matches[4];
    // TODO: fix timezone
    return strtotime($date);
  }
}