<?php

class DrupalOrgCrawlerCommitsPage
{
  private $_doc = NULL;

  function __construct($html) {
    $this->_doc = new DOMDocument();
    @$this->_doc->loadHTML($html);
  }

  function getCommits() {
    $commits = array();
    $xpath = new DOMXpath($this->_doc);
    $matches = $xpath->query("//div[@class='view-content']//li");

    foreach ($matches as $commit_node) {
      $commit = new DrupalOrgCrawlerCommit($this->_doc, $commit_node);
      $commits[] = $commit;
    }

    return $commits;
  }
}

class DrupalOrgCrawlerCommit
{
  private $_node = NULL;
  private $_doc = NULL;

  function __construct(&$doc, &$node) {
    $this->_node = $node;
    $this->_doc = $doc;
  }

  function getId() {
    $id = NULL;
    $xpath = new DOMXpath($this->_doc);
    $matches = $xpath->query("div//div[@class='commit-info']//strong/a", $this->_node);
    if ($matches->length > 0) {
      $id = $matches->item(0)->nodeValue;
    }

    return $id;
  }

  function getAuthor() {
    $author = NULL;
    $xpath = new DOMXpath($this->_doc);
    $matches = $xpath->query("div//div[@class='attribtution']/span[@class='authored-by']/a", $this->_node);

    if ($matches->length == 0) {
      $matches = $xpath->query("div//div[@class='attribtution']/a", $this->_node);
    }

    if ($matches->length > 0) {
      $author = trim($matches->item(0)->nodeValue);
    }

    return $author;
  }

  function getCommitter() {
    $author = NULL;
    $xpath = new DOMXpath($this->_doc);
    $matches = $xpath->query("div//div[@class='attribtution']/span[@class='committed-by']/a", $this->_node);

    if ($matches->length == 0) {
      $matches = $xpath->query("div//div[@class='attribtution']/a", $this->_node);
    }

    if ($matches->length > 0) {
      $author = trim($matches->item(0)->nodeValue);
    }

    return $author;
  }

  function getProject() {
    $project = NULL;
    $xpath = new DOMXpath($this->_doc);
    $matches = $xpath->query("div//div[@class='commit-global']/h3/a[1]", $this->_node);
    if ($matches->length > 0) {
      $project = trim($matches->item(0)->nodeValue);
    }

    return $project;
  }

  function getBranch() {
    $branch = NULL;
    $xpath = new DOMXpath($this->_doc);
    $matches = $xpath->query("div//div[@class='commit-info']//strong[2]", $this->_node);
    if ($matches->length > 0) {
      $branch = trim($matches->item(0)->nodeValue);
    }

    return $branch;
  }

  function getCommitTimestamp() {
    $timestamp = NULL;
    $xpath = new DOMXpath($this->_doc);
    $matches = $xpath->query("div//div[@class='commit-global']/h3/a[2]", $this->_node);
    if ($matches->length > 0) {
      $timestamp = strtotime($matches->item(0)->nodeValue);
    }

    return $timestamp;
  }

  function getMessage() {
    $branch = NULL;
    $xpath = new DOMXpath($this->_doc);
    $matches = $xpath->query("div//span[@class='field-content']//pre", $this->_node);
    if ($matches->length > 0) {
      $branch = trim($matches->item(0)->nodeValue);
    }

    return $branch;
  }
}