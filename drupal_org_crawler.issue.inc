<?php

class DrupalOrgCrawlerIssue
{
  private $_doc = NULL;

  function __construct($html) {
    $this->_doc = new DOMDocument();
    @$this->_doc->loadHTML($html);
  }

  function getTitle() {
    $status = NULL;
    $xpath = new DOMXpath($this->_doc);
    $matches = $xpath->query("//h1");
    if ($matches->length > 0) {
      $status = trim($matches->item(0)->nodeValue);
    }

    return $status;
  }

  function getAuthor() {
    $author = NULL;
    $xpath = new DOMXpath($this->_doc);
    $matches = $xpath->query("//div[contains(@class, 'node')]//div[@class='submitted']/a");
    if ($matches->length > 0) {
      $author = trim($matches->item(0)->nodeValue);
    }

    return $author;
  }

  function getCreatedTimestamp() {
    $timestamp = NULL;
    $xpath = new DOMXpath($this->_doc);
    $matches = $xpath->query("//div[contains(@class, 'node')]//div[@class='submitted']/em");
    if ($matches->length > 0) {
      $timestamp = $this->parseDate($matches->item(0)->nodeValue);
    }

    return $timestamp;
  }

  function getCategory() {
    $category = NULL;
    $xpath = new DOMXpath($this->_doc);
    $matches = $xpath->query("//div[contains(@class, 'node')]//div[contains(@class, 'summary')]/table/tbody/tr[./td[contains(., 'Category')]]/td");
    if ($matches->length > 1) {
      $category = trim($matches->item(1)->nodeValue);
    }

    return $category;
  }

  function getProject() {
    $project = NULL;
    $xpath = new DOMXpath($this->_doc);
    $matches = $xpath->query("//div[contains(@class, 'node')]//div[contains(@class, 'summary')]/table/tbody/tr[./td[contains(., 'Project')]]/td");
    if ($matches->length > 1) {
      $project = trim($matches->item(1)->nodeValue);
    }

    return $project;
  }

  function getStatus() {
    $status = NULL;
    $xpath = new DOMXpath($this->_doc);
    $matches = $xpath->query("//div[contains(@class, 'node')]//div[contains(@class, 'summary')]/table/tbody/tr[./td[contains(., 'Status')]]/td");
    if ($matches->length > 1) {
      $status = trim($matches->item(1)->nodeValue);
    }

    return $status;
  }

  function hasPatch() {
    $has_path = FALSE;
    $xpath = new DOMXpath($this->_doc);
    $matches = $xpath->query("//div[contains(@class, 'node')]//div[contains(@class, 'summary')]//table//a[contains(@href, '.patch')]");
    if ($matches->length > 0) {
      $has_path = TRUE;
    }

    return $has_path;
  }

  function getComments() {
    $comments = array();
    $xpath = new DOMXpath($this->_doc);
    $matches = $xpath->query("//div[@id='comments']//div[contains(@class, 'comment-inner')]");

    foreach ($matches as $node) {
      $comment = new DrupalOrgCrawlerComment($this->_doc, $node);
      $comments[] = $comment;
    }

    return $comments;
  }

  private function parseDate($date) {
    $matches = array();
    preg_match('/([a-zA-Z]+)\s+([0-9]+),\s+([0-9]+)\sat\s(.*)/', $date, $matches);

    $date = $matches[1] . '-' . $matches[2] . '-' . $matches[3] . ' ' . $matches[4];
    // TODO: fix timezone
    return strtotime($date);
  }
}

class DrupalOrgCrawlerUserIssues
{
  private $_doc = NULL;

  function __construct($html) {
    $this->_doc = new DOMDocument();
    @$this->_doc->loadHTML($html);
  }

  function getIssuesIds() {
    $issues = array();
    $xpath = new DOMXpath($this->_doc);
    $matches = $xpath->query("//table[contains(@class, 'views-table')]/tbody/tr/td[contains(@class, 'views-field-title')]/a");
    foreach ($matches as $match) {
      $issue_link = trim($match->getAttribute('href'));
      $issues[] = _drupal_org_crawler_get_issue_id($issue_link);
    }

    return $issues;
  }
}