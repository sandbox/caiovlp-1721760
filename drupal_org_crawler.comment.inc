<?php

class DrupalOrgCrawlerComment
{
  private $_node = NULL;
  private $_doc = NULL;

  function __construct(&$dom, &$node) {
    $this->_node = $node;
    $this->_doc = $dom;
  }

  function getNumber() {
    $number = NULL;
    $xpath = new DOMXpath($this->_doc);
    $matches = $xpath->query("div//h3/a", $this->_node);
    if ($matches->length > 0) {
      $number = preg_replace('/#([0-9]+)/', '$1', $matches->item(0)->nodeValue);
    }

    return $number;
  }

  function getAuthor() {
    $author = NULL;
    $xpath = new DOMXpath($this->_doc);
    $matches = $xpath->query("div//div[@class='submitted']/a", $this->_node);
    if ($matches->length > 0) {
      $author = trim($matches->item(0)->nodeValue);
    }

    return $author;
  }

  function getCreatedTimestamp() {
    $timestamp = NULL;
    $xpath = new DOMXpath($this->_doc);
    $matches = $xpath->query("div//div[@class='submitted']/em", $this->_node);
    if ($matches->length > 0) {
      $timestamp = $this->parseDate($matches->item(0)->nodeValue);
    }

    return $timestamp;
  }

  function hasPatch() {
    $has_path = FALSE;
    $xpath = new DOMXpath($this->_doc);
    $matches = $xpath->query("div//table//a[contains(@href, '.patch')]", $this->_node);
    if ($matches->length > 0) {
      $has_path = TRUE;
    }

    return $has_path;
  }

  private function parseDate($date) {
    $matches = array();
    preg_match('/([a-zA-Z]+)\s+([0-9]+),\s+([0-9]+)\sat\s(.*)/', $date, $matches);

    $date = $matches[1] . '-' . $matches[2] . '-' . $matches[3] . ' ' . $matches[4];
    // TODO: fix timezone
    return strtotime($date);
  }
}