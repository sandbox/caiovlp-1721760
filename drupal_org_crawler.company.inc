<?php

class DrupalOrgCrawlerCompany
{
  private $_doc = NULL;

  function __construct($html) {
    $this->_doc = new DOMDocument();
    @$this->_doc->loadHTML($html);
  }

  function getTitle() {
    $status = NULL;
    $xpath = new DOMXpath($this->_doc);
    $matches = $xpath->query("//h1");
    if ($matches->length > 0) {
      $status = trim($matches->item(0)->nodeValue);
    }

    return $status;
  }

  function getAuthor() {
    $author = NULL;
    $xpath = new DOMXpath($this->_doc);
    $matches = $xpath->query("//div[contains(@class, 'node')]//div[@class='submitted']/a");
    if ($matches->length > 0) {
      $author = trim($matches->item(0)->nodeValue);
    }

    return $author;
  }

  function getCreatedTimestamp() {
    $timestamp = NULL;
    $xpath = new DOMXpath($this->_doc);
    $matches = $xpath->query("//div[contains(@class, 'node')]//div[@class='submitted']/em");
    if ($matches->length > 0) {
      $timestamp = $this->parseDate($matches->item(0)->nodeValue);
    }

    return $timestamp;
  }

  function getUsers() {
    $users = array();
    $xpath = new DOMXpath($this->_doc);
    $matches = $xpath->query("//div[contains(@class, 'node')]//h3/a");

    if (!is_null($matches) && $matches->length > 0) {
      foreach ($matches as $node) {
        $uid = $this->getUserId($node->getAttribute('href'));
        $users[$uid] = $node->nodeValue;
      }
    }

    return $users;
  }

  private function getUserId($user_url) {
    $user_id = NULL;

    $match = strrchr($user_url, '/');
    if ($match !== FALSE) {
      $user_id = substr($match, 1);
    }

    return $user_id;
  }

  private function parseDate($date) {
    $matches = array();
    preg_match('/([a-zA-Z]+)\s+([0-9]+),\s+([0-9]+)\sat\s(.*)/', $date, $matches);

    $date = $matches[1] . '-' . $matches[2] . '-' . $matches[3] . ' ' . $matches[4];
    // TODO: fix timezone
    return strtotime($date);
  }
}